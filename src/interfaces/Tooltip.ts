import {ReactNode} from "react";

export type TooltipProps = {
    options: string[],
    isOpen: boolean,
    children: ReactNode
}