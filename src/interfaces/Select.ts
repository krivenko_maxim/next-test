import {ChangeEventHandler} from "react";

export type SelectProps = {
    options: string[],
    onChange: ChangeEventHandler<HTMLSelectElement>
}