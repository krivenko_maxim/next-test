export type Car = {
    id: string,
    title: string,
    year: number | string,
    vin: string,
    imageUrls: string[] | undefined,
    engineCapacity: number,
    engineTransmission: string,
    enginePower: string,
    fuelType: string,
    price: number,
    mileage: number | undefined,
    baseOptions: string[] | undefined,
    color: string | undefined
}