import {Popover} from "react-tiny-popover";
import {TooltipProps} from "@/interfaces/Tooltip";
import {Swiper, SwiperSlide} from "swiper/react";
import {FreeMode, Mousewheel, Scrollbar} from "swiper";
import 'swiper/scss'
import 'swiper/scss/free-mode'
import 'swiper/scss/scrollbar'
import styles from '@/components/OptionsPopover/OptionsPopover.module.scss'

export default function OptionsPopover({options, isOpen, children}: TooltipProps) {
    return <Popover
        align='end'
        isOpen={isOpen}
        positions={['top']}
        containerClassName={styles.container}
        content={_ => {
            return (
                <div className={styles.wrapper}>
                    <p className={styles.title}>
                        Пакеты опций
                    </p>
                    <Swiper
                        style={{zIndex: 3, height: '2rem'}}
                        direction='vertical'
                        slidesPerView='auto'
                        freeMode
                        mousewheel
                        modules={[FreeMode, Scrollbar, Mousewheel]}
                        className={styles.swiper}
                    >
                            {options.map((option, index) =>
                                <SwiperSlide
                                    className={styles.swiperSlide}
                                    key={index}
                                >
                                        <span className={styles.option}>
                                    {option}
                                </span>
                                </SwiperSlide>)
                            }
                    </Swiper>
                </div>
            )
        }}>
        {children}
    </Popover>
}