import {SelectProps} from "@/interfaces/Select";
import styles from '@/components/Select/Select.module.scss'
export function Select({options, onChange}: SelectProps) {
    return <select onChange={onChange} className={styles.select}>
        {options?.map((option, index) => {
            return <option label={option} value={index} key={index} className={styles.option}/>
        })}
    </select>
}