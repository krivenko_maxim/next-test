import styles from '@/components/CarCard/CarCard.module.scss'
import {Car} from "@/interfaces/Car";
import {prettifyNumber} from "@/util/prettifyNumber";
import {useState} from "react";
import OptionsTooltip from "@/components/OptionsPopover/OptionsPopover";

import {Swiper, SwiperSlide} from "swiper/react";
import {FreeMode, Navigation} from "swiper";
import "swiper/scss";
import "swiper/scss/free-mode";

export default function CarCard({
                                    title,
                                    engineCapacity,
                                    engineTransmission,
                                    enginePower,
                                    price,
                                    fuelType,
                                    vin,
                                    year,
                                    mileage,
                                    imageUrls,
                                    baseOptions,
                                    color
                                }: Car
) {
    const [isHovered, setIsHovered] = useState<boolean>(false)
    const [areOptionsOpen, setOptionsOpen] = useState<boolean>(false)

    return <div
        className={styles.card}
        onMouseEnter={() => setIsHovered(true)}
        onMouseLeave={() => setIsHovered(true)}
    >
        <div className={styles.titleBlock}>
            <h4 className={styles.title}>
                {title}
            </h4>
            <span className={styles.year}>
                {year}
            </span>
        </div>
        <p className={styles.vin}>
            {vin}
        </p>
        <Swiper
            className={styles.gallery}
            slidesPerView='auto'
            spaceBetween={0}
            freeMode
            modules={[FreeMode, Navigation]}
        >
            {imageUrls?.map((url, index) => {
                return (
                    <SwiperSlide className={styles.img} key={url}>
                        <div className={styles.img}>
                            <img
                                src={url}
                                alt='image'
                                style={{display: 'block', height: '100%'}}
                            />
                        </div>
                    </SwiperSlide>
                )
            })}
        </Swiper>
        <div className={styles.options}>
            <div className={styles.option}>
                <span className={styles.optionTitle}>
                    ДВИГАТЕЛЬ
                </span>
                <span className={styles.optionContent}>
                    {engineCapacity} л / {enginePower} лс / {fuelType}
                </span>
            </div>
            <div className={styles.option}>
                <span className={styles.optionTitle}>
                    КПП
                </span>
                <span className={styles.optionContent}>
                    {engineTransmission}
                </span>
            </div>
            {mileage &&
                <div className={styles.option}>
                    <span className={styles.optionTitle}>
                        ПРОБЕГ
                    </span>
                    <span className={styles.optionContent}>
                        {mileage} км
                    </span>
                </div>
            }
            {isHovered && color &&
                <div className={styles.option}>
                    <span className={styles.optionTitle}>
                        ЦВЕТ
                    </span>
                    <span className={styles.optionContent}>
                        {color}
                    </span>
                </div>
            }
            {
                baseOptions && baseOptions.length !== 0 && isHovered &&
                <div className={styles.option}>
                    <span className={styles.optionTitle}>
                        ПАКЕТЫ ОПЦИЙ
                    </span>
                    <div className={`${styles.optionContent} `}>
                        <OptionsTooltip options={baseOptions} isOpen={areOptionsOpen}>
                            <div className={`${styles.visibleOptions} ${styles.abc}`}>
                                <div className={styles.clippedText}>
                                    {baseOptions[0] ?? ''}
                                </div>
                                <button
                                    onClick={() => setOptionsOpen(areOpen => !areOpen)}
                                    className={`${styles.otherOptionsButton} ${styles.optionContent}`}
                                >
                                    + еще {baseOptions.filter(option => option !== null).length - 1}
                                </button>
                            </div>
                        </OptionsTooltip>
                    </div>
                </div>
            }

        </div>
        <div className={styles.buy}>
            <span className={styles.price}>
                {prettifyNumber(price)} <span>&#x20BD;</span>
            </span>
            <button className={styles.button}>
                КУПИТЬ
            </button>
        </div>

    </div>
}
