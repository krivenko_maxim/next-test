import Head from 'next/head'
import styles from '@/pages/Home.module.scss'
import CarCard from "@/components/CarCard/CarCard";
import {Col, Container, Row} from "react-bootstrap";
import {dehydrate, QueryClient} from "@tanstack/query-core";
import {Car} from "@/interfaces/Car";
import {useQuery} from "@tanstack/react-query";
import {Select} from "@/components/Select/Select";
import {useState} from "react";

type HomeProps = {
    dehydratedState: any,
    brands: string[],
    initialBrandIndex: number,
}

export default function Home({dehydratedState, brands, initialBrandIndex,}: HomeProps) {
    const [selectedBrandIndex, setSelectedBrandIndex] = useState<number>(initialBrandIndex)

    const {data, isFetched} = useQuery<Car[]>({
        queryKey: ['cars', selectedBrandIndex],
        queryFn: () => loader(brands[selectedBrandIndex]),
        initialData: dehydratedState
    })
    return (
        <>
            <Head>
                <title>Cars</title>
                <meta name="description" content="Cars"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            <main>
                <Select options={brands} onChange={(e) => setSelectedBrandIndex(e.target.selectedIndex)}/>
                {isFetched &&
                    <Container fluid className={styles.container}>
                        <Row className={styles.row}>
                            {data?.map(elem => {
                                return <Col key={elem.id}>
                                    <CarCard
                                        {...elem}
                                    />
                                </Col>
                            })}
                        </Row>
                    </Container>
                }
            </main>
        </>
    )
}

async function loader(brand: string): Promise<Car[]> {
    const data = await fetch(`https://maximum.expert/api/stock-test?brand=${brand}`).then(data => data.json())
    return data.list.map((car) => {
        return {
            id: car._id,
            title: `${car.feedData.brandName} ${car.feedData.modelName} ${car.feedData.equipmentVariantName}`,
            year: car.feedData.modelYear,
            vin: car.feedData.vin,
            imageUrls: car.photobank.imgs.map(img => img.url),
            engineCapacity: car.feedData.engine.engineCapacity,
            engineTransmission: car.feedData.equipmentVariantTransmissionType,
            enginePower: car.feedData.engine.enginePower,
            fuelType: car.feedData.equipmentVariantFuelType,
            price: car.feedData.autoPrice,
            mileage: 16000,
            baseOptions: car.feedData.baseOptions?.map(option => option.universalOptions?.[0]?.name || null),
            color: car.feedData?.colorByClassifierName || null
        } as Car
    })
}

export async function getServerSideProps() {
    const brands = ['Audi', 'Mitsubishi', 'Volkswagen', 'Kia', 'Honda', 'Hyundai']
    const initialBrandIndex = 0
    const queryClient = new QueryClient({
        defaultOptions: {
            queries: {
                staleTime: Infinity
            }
        }
    })
    await queryClient.prefetchQuery(['cars', 0],
        async () => loader(brands[initialBrandIndex]))

    return {
        props: {
            dehydratedState: dehydrate(queryClient),
            brands,
            initialBrandIndex
        }
    }
}
